package no.ntnu.imageprocessing.activegesturerecognition.activities;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import no.ntnu.imageprocessing.activegesturerecognition.R;
import no.ntnu.imageprocessing.activegesturerecognition.custom_views.GestureView;
import no.ntnu.imageprocessing.activegesturerecognition.custom_views.GestureView.MODE;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.GestureRecognitionData;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

public class GestureRecognition extends AppCompatActivity {

    private TextView gestureRecognized;
    private TextView gestureInfo;

    static {
        System.loadLibrary("tensorflow_inference");
    }

    private TensorFlowInferenceInterface inferenceInterface;

    private static final String MODEL_FILE = "file:///android_asset/retrained_graph_5_8000.pb";
    private static final String inputName = "input";
    private static final String outputName = "final_result";
    private List<GestureRecognitionData> data;
    private Vector<String> labels = new Vector<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture_recognition);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final GestureView gestureView = findViewById(R.id.gesture_draw_view);
        gestureView.setMode(MODE.GESTURE_RECOGNITION);
        gestureView.setGesture("RecognitionData");

        new Thread(new Runnable() { //Lambda is a no-go as it breaks the code.
            @Override
            public void run() {
                inferenceInterface = new TensorFlowInferenceInterface(GestureRecognition.this.getAssets(), MODEL_FILE);

                //SystemClock.sleep(3000);
                boolean shouldRum = true;
                String lastPath = gestureView.getPathToLastGesture();
                long init = SystemClock.currentThreadTimeMillis();

                String actualFilename = "retrained_labels_5_8000.txt";
                Log.i("Blah blah blah", "Reading labels from: " + actualFilename);
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new InputStreamReader(GestureRecognition.this.getAssets().open(actualFilename)));
                    String line;
                    while ((line = br.readLine()) != null) {
                        labels.add(line);
                    }
                    br.close();
                } catch (IOException e) {
                    throw new RuntimeException("Problem reading label file!", e);
                }

                while (shouldRum) {
                    String newPath = gestureView.getPathToLastGesture();
                    if (!Objects.equals(lastPath, newPath)) {
                        lastPath = newPath;
                        Log.d("Blah blah blah", "");
                        if (!lastPath.equals("")) {
                            Bitmap bitmap = gestureView.getBitmap();

                            int inputSize = 224;
                            int intValueSize = inputSize * inputSize;
                            int floatValueSize = intValueSize * 3;
                            int[] intValues = new int[intValueSize];
                            float[] floatValues = new float[floatValueSize];

                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 224, 224, false);
                            resizedBitmap.getPixels(intValues, 0, inputSize, 0, 0, inputSize, inputSize);

                            // Sligtly modified codeblock from tensorflow example code: TensorflowImageClassifier.java, lines 132-140
                            // https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/android/src/org/tensorflow/demo/TensorFlowImageClassifier.java
                            // Apache-2.0 lisenced - Fair reuse
                            for (int i = 0; i < intValues.length; ++i) {
                                final int val = intValues[i];
                                floatValues[i * 3] = (((val >> 16) & 0xFF) - 127) / 1F;
                                floatValues[i * 3 + 1] = (((val >> 8) & 0xFF) - 127) / 1F;
                                floatValues[i * 3 + 2] = ((val & 0xFF) - 127) / 1F;
                            }

                            float[] outputs = new float[labels.size()];
                            String[] outputNames = new String[]{outputName};
                            inferenceInterface.feed(inputName, floatValues, 1, 224, 224, 3);
                            inferenceInterface.run(outputNames);
                            inferenceInterface.fetch(outputName, outputs);

                            data = new ArrayList<>();

                            for (int i = 0; i < labels.size(); i++) {
                                data.add(new GestureRecognitionData(labels.get(i), outputs[i]));
                            }

                            sortGesturesByProbability();
                            Log.d("Blah blah blah", "Sorted on probability");
                            printGestures();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    gestureRecognized.setText(data.get(0).getGesture());
                                }
                            });
                        }
                    }

                    SystemClock.sleep(100);

                    // Causes thread to quit after 10 minuttes
                    if (SystemClock.currentThreadTimeMillis() - init > 600000) {
                        shouldRum = false;
                    }
                }
            }
        }).start();



        Button btnCorrect = findViewById(R.id.ges_rec_correct);
        btnCorrect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Write to a file that gesture was correctly classified, format must be easy to process in a spreadsheet
                if (!gestureView.isDrawn()) {
                    Toast.makeText(GestureRecognition.this, "No gesture recognized to record.", Toast.LENGTH_SHORT).show();
                } else {
                    File iDir = DirUtil.getGestureDir("result");
                    String filepath = iDir.getPath() + "/data.csv";
                    File gestureFile = new File(filepath);
                    String correct = gestureRecognized.getText().toString();
                    if (!gestureFile.exists()) {
                        try {
                            gestureFile.createNewFile();

                            FileWriter fw = new FileWriter(gestureFile);
                            BufferedWriter bw = new BufferedWriter(fw);
                            PrintWriter pw = new PrintWriter(bw);
                            fw.write(csvTitleRow());
                            pw.close();
                            bw.close();
                            fw.close();
                        } catch (IOException e) {
                            Toast.makeText(GestureRecognition.this, "Unable to record the data.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    try {
                        FileWriter fw = new FileWriter(gestureFile, true);
                        BufferedWriter bw = new BufferedWriter(fw);
                        PrintWriter pw = new PrintWriter(bw);
                        fw.write(csvRow(correct, true));
                        pw.close();
                        bw.close();
                        fw.close();
                        gestureView.emptyCanvas();
                    } catch (Exception e) {
                        Toast.makeText(GestureRecognition.this, "Unable to record the data.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        Button btnUnsure = findViewById(R.id.ges_rec_unsure);
        btnUnsure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*probably only needed if the user makes an obvious error, like starts doodling on the canvas.
                * Do NOT save this in a file
                * */
                gestureView.emptyCanvas();
                Toast.makeText(GestureRecognition.this, "Redraw gesture for recognition", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnWrong = findViewById(R.id.ges_rec_wrong);
        btnWrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.select_dialog_singlechoice);
                for (int i = 0; i < labels.size(); i++) {
                    arrayAdapter.add(labels.get(i));
                }

                /*TODO Write to a file that gesture was incorrectly classified with another, format must be easy to process in a spreadsheet
                * An alert dialog should be presented to the user where the user selects the correct gesture from a list in the dialog.
                * */
                if (!gestureView.isDrawn()) {
                    Toast.makeText(GestureRecognition.this, "No gesture recognized to record.", Toast.LENGTH_SHORT).show();
                } else {
                    final TextView identifier = new TextView(GestureRecognition.this);
                    new AlertDialog.Builder(GestureRecognition.this)
                            .setTitle("Select gesture")
                            .setSingleChoiceItems((labels.toArray(new String[labels.size()])), 0, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    identifier.setText(labels.get(which));
                                }
                            })
                            .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    File iDir = DirUtil.getGestureDir("result");
                                    String filepath = iDir.getPath() + "/data.csv";
                                    File gestureFile = new File(filepath);
                                    if (!gestureFile.exists()) {
                                        try {
                                            gestureFile.createNewFile();

                                            FileWriter fw = new FileWriter(gestureFile, true);
                                            BufferedWriter bw = new BufferedWriter(fw);
                                            PrintWriter pw = new PrintWriter(bw);
                                            fw.write(csvTitleRow());
                                            pw.close();
                                            bw.close();
                                            fw.close();
                                        } catch (IOException e) {
                                            Toast.makeText(GestureRecognition.this, "Unable to record the data.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    try {
                                        FileWriter fw = new FileWriter(gestureFile, true);
                                        BufferedWriter bw = new BufferedWriter(fw);
                                        PrintWriter pw = new PrintWriter(bw);
                                        fw.write(csvRow(identifier.getText().toString(), false));
                                        pw.close();
                                        bw.close();
                                        fw.close();
                                        gestureView.emptyCanvas();
                                    } catch (Exception e) {
                                        Toast.makeText(GestureRecognition.this, "Unable to record the data.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            })
                            .show();

                }
            }
        });

        gestureRecognized = findViewById(R.id.ges_rec_classified_gesture);
        gestureInfo = findViewById(R.id.ges_rec_info);
    }

    private String suggestedGestureName() {
        return data.get(0).getGesture();
    }

    private Float suggestedGestureProbability() {
        return data.get(0).getProbability();
    }

    private String cell(String what){
        return what + ",";
    }

    /**
     * TODO these methods will be used to generate the csv rows. Note its not compatible with csvTitleRow() yet since data list was sorted which probably will be undone later
     * @param correct
     * @return
     */
    private String csvRow(String drawn, boolean correct){
        sortGesturesByLabels();
        StringBuilder res = new StringBuilder(cell(drawn));
        res.append(cell(gestureRecognized.getText().toString()));
        res.append(cell(Boolean.toString(correct)));
        for (int i = 0; i < data.size(); i++) {
            res.append(cell(Float.toString(data.get(i).getProbability())));
        }
        res.append("\n");
        return res.toString();
    }

    private String csvTitleRow(){
        sortGesturesByLabels();
        StringBuilder res = new StringBuilder(cell("Drawn"));
        res.append(cell("Recognized"));
        res.append(cell("Correct"));
        for (int i = 0; i < data.size(); i++) {
            res.append(cell(data.get(i).getGesture()));
        }
        res.append("\n");
        return res.toString();
    }


    private void sortGesturesByProbability(){
        data.sort((o1, o2) -> o1.getProbability() < o2.getProbability() ? 1 : o1.getProbability() == o2.getProbability() ? 0 : -1);
    }

    private void sortGesturesByLabels(){
        data.sort((o1,o2)-> o1.getGesture().compareTo(o2.getGesture()));
    }

    private void printGestures(){
        for (int i = 0; i < data.size(); i++) {
            Log.d("Blah blah blah", data.get(i).toString());
        }
    }



    private GestureRecognitionData findElementByName(String gestureName){
        for (int i = 0; i < data.size(); i++) {
            if(data.get(i).getGesture().equals(gestureName)){
                return data.get(i);
            }
        }

        Log.e("Blah blah blah", "Find operation didn't find gesture - your code is wrong");
        return null;
    }
}
