package no.ntnu.imageprocessing.activegesturerecognition.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;

import java.io.File;

import no.ntnu.imageprocessing.activegesturerecognition.R;
import no.ntnu.imageprocessing.activegesturerecognition.custom_views.GestureView;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil;

public class FtGeneration extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    NavigationView navigationView;
    private Button prevbtn, thisbtn, allbtn;
    Bundle data;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_gesture);

        GestureView gestureView = findViewById(R.id.gesture_draw_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        prevbtn = (Button) findViewById(R.id.button4);
        thisbtn = (Button) findViewById(R.id.button);
        allbtn = (Button) findViewById(R.id.button3);

        TextView topText = findViewById(R.id.trainGestureTextAboveBtn);
        topText.setText("Draw to generate Images and FTs");

        TextView btmText = findViewById(R.id.traingGestureTextAboveCanvas);
        btmText.setText("App will crash after 7-15 drawn paths");

        prevbtn.setVisibility(View.GONE);
        thisbtn.setVisibility(View.GONE);
        allbtn.setVisibility(View.GONE);

        prevbtn.setOnClickListener(this);
        thisbtn.setOnClickListener(this);
        allbtn.setOnClickListener(this);

        String name = "GesturesWithFt";
        gestureView.setGesture(name);
        toolbar.setTitle(name);
        gestureView.setMode(GestureView.MODE.GESTURE_FT_GENERATION);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String thisGesture = data.getString("gesture");
        File file = DirUtil.getGestureDir(thisGesture);
        GestureView gestureView = findViewById(R.id.gesture_draw_view);
        Boolean isDrawn = gestureView.isDrawn();
        File deleteFile;
        String path, oldPath, newPath;

        String fileType = ".jpg";

        if(file.listFiles().length == 0){
            Toast.makeText(this, "No gestures to remove", Toast.LENGTH_SHORT).show();
        }else {
            switch (v.getId()) {
                case R.id.button:               // remove the current image
                    if (!isDrawn) {
                        Toast.makeText(this, "No gestures to remove", Toast.LENGTH_SHORT).show();
                    } else {
                        int fileName = file.listFiles().length - 1;
                        path = file.getPath() + "/" + fileName + fileType;
                        deleteFile = new File(path);
                        deleteFile.delete();

                        gestureView.emptyCanvas();
                        Toast.makeText(this, "Gesture removed", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.button3:              // remove all the images for this gesture
                    int fileLength = file.listFiles().length;
                    for (int i = 0; i < fileLength; i++) {
                        new File(file.getPath() + "/" + i + fileType).delete();
                    }
                    gestureView.emptyCanvas();
                    Toast.makeText(this, "All gestures for " + thisGesture + " removed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button4:              // remove previous image from this gesture
                    int lastFile = file.listFiles().length - 1;
                    int secondFile = file.listFiles().length - 2;
                    if (!isDrawn) {
                        path = file.getPath() + "/" + lastFile + fileType;
                        deleteFile = new File(path);
                        deleteFile.delete();
                        Toast.makeText(this, "Gesture " + lastFile + ".png removed", Toast.LENGTH_SHORT).show();
                    } else {
                        if (lastFile == 0) {
                            Toast.makeText(this, "No gestures to remove", Toast.LENGTH_SHORT).show();
                            break;
                        }
                        path = file.getPath() + "/" + secondFile + fileType;
                        deleteFile = new File(path);
                        deleteFile.delete();

                        //rename last file to second last
                        oldPath = file.getPath() + "/" + lastFile + fileType;
                        newPath = file.getPath() + "/" + secondFile + fileType;
                        File renameFile = new File(oldPath);
                        File newFile = new File(newPath);
                        renameFile.renameTo(newFile);
                        Toast.makeText(this, "Gesture " + secondFile + ".png removed", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

    }
}
