package no.ntnu.imageprocessing.activegesturerecognition.utilities;

import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Created by Tor-Martin Holen on 14-Nov-17.
 */

public abstract class DirUtil {
    public static File getGestureDir(String gestureName) {
        File parent = getRootDir();
        File gesture = new File(parent, gestureName);

        if (!gesture.mkdirs()) {
            Log.d("DirError", "Directory not created or already existing");
        }

        return gesture;
    }

    public static File getRootDir() {
        File root = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "AGR");
        if (!root.mkdirs()) {
            Log.d("DirError", "Directory not created or already existing");
        }
        return root;
    }

    public static File renameTo(String oldName, String newName){
        File parent = getRootDir();
        File gesture = new File(parent, oldName);
        File newFolder = new File(parent, newName);

        if (!newFolder.mkdirs()) {
            Log.d("DirError", "Directory not created or already existing");
        }

        if(gesture.exists()) {
            gesture.renameTo(newFolder);
        }

        return gesture;
    }

    public static boolean delete(String gestureName){
        File parent = getRootDir();
        File gesture = new File(parent, gestureName);

        if(gesture.exists()) {
            int fileLength = gesture.listFiles().length;
            for (int i = 0; i < fileLength; i++) {
                new File(gesture.getPath() + "/" + i + ".png").delete();
            }
            if(gesture.delete()){
                return true;
            }
        }
        return false;
    }
}
