package no.ntnu.imageprocessing.activegesturerecognition.utilities;

/**
 * Created by Tor-Martin Holen on 29-Nov-17.
 */

public class GestureRecognitionData {
    private String gesture;
    private float probability;

    public GestureRecognitionData(String gesture, float probability) {
        this.gesture = gesture;
        this.probability = probability;
    }

    public String getGesture() {
        return gesture;
    }

    public float getProbability() {
        return probability;
    }

    @Override
    public String toString() {
        return "Gesture:" + gesture + "\t\tProbability:" + probability;
    }
}
