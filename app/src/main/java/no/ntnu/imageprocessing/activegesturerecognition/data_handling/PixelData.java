package no.ntnu.imageprocessing.activegesturerecognition.data_handling;

/**
 * Created by Tor-Martin Holen on 22-Oct-17.
 */

public class PixelData{
    private float x, y;
    private long t;

    public PixelData(float x, float y, long t) {
        this.x = x;
        this.y = y;
        this.t = t;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public long getT() {
        return t;
    }
}