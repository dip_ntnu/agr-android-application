package no.ntnu.imageprocessing.activegesturerecognition.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.util.ArrayList;

import no.ntnu.imageprocessing.activegesturerecognition.R;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.GestureAdapter;

public class Gestures extends AppCompatActivity {
    public static final int WRITE_ACCESS = 45639;
    private ListView gestureList;
    private GestureAdapter gestureAdapter;
    static ArrayList<String> gst = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestures);

        requestPermission();

        gestureList = findViewById(R.id.listData);

        populateGestures(gst);
        gestureAdapter = new GestureAdapter(this, 0, gst);
        gestureList.setAdapter(gestureAdapter);
        registerForContextMenu(gestureList);


        gestureList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), TrainGesture.class);
                i.putExtra("gesture", (String) gestureList.getItemAtPosition(position));
                startActivity(i);
            }

        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!OpenCVLoader.initDebug()) {
            Log.e(this.getClass().getSimpleName(), "  OpenCVLoader.initDebug(), not working.");
        } else {
            Log.d(this.getClass().getSimpleName(), "  OpenCVLoader.initDebug(), working.");
        }
    }

    private void populateGestures(ArrayList<String> gst) {
        File root = DirUtil.getRootDir();
        if (root.exists()) {
            File[] gestures = root.listFiles();
            if (gestures != null) {
                for (File file : gestures) {
                    if (!file.getName().contains("FT-Magnitude-") && !file.getName().contentEquals("result") && !file.getName().contains(".zip") && !file.getName().contains("RecognitionData")){
                        gst.add(file.getName());
                    }
                }
            } else {
                Toast.makeText(this, "Get started by adding a gesture up in the toolbar", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.gesture_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.toolbar_add_gesture) {
            addGestureDialog();
            Toast.makeText(this, "Add gesture", Toast.LENGTH_SHORT).show();

            return true;
        } else if (id == R.id.toolbar_recognition_test) {
            Intent i = new Intent(getApplicationContext(), GestureRecognition.class);
            startActivity(i);
            return true;
        } else if (id == R.id.toolbar_ft_generation) {
            Intent i = new Intent(getApplicationContext(), FtGeneration.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void addGestureDialog() {
        final EditText identifier = new EditText(this);
        new AlertDialog.Builder(this)
                .setTitle("Create gesture")
                .setMessage("Select a identifying name for the gesture. ")
                .setView(identifier)
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String result = identifier.getText().toString();
                        DirUtil.getGestureDir(result);
                        gestureAdapter.add(result);
                        gestureAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_ACCESS);
            }
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gesture_menu_options, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        File root = DirUtil.getRootDir();
        final File[] gestures = root.listFiles();
        final String gestureName = gestures[info.position].getName();
        switch(item.getItemId()){
            case R.id.delete_id:
                if(DirUtil.delete(gestureName)){
                    gst.remove(info.position);
                    gestureAdapter.notifyDataSetChanged();
                    Toast.makeText(this, "Gesture "+gestureName+" removed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rename_id:
                final EditText identifier = new EditText(this);
                identifier.setText(gestureName);
                new AlertDialog.Builder(this)
                        .setTitle("Edit gesture Name")
                        .setMessage("Select a identifying name for the gesture. ")
                        .setView(identifier)
                        .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String result = identifier.getText().toString();
                                DirUtil.renameTo(gestureName, result);
                                gst.set(info.position, result);
                                gestureAdapter.notifyDataSetChanged();
                                Toast.makeText(Gestures.this, "Gesture renamed", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        })
                        .show();
                break;
        }
        return super.onContextItemSelected(item);
    }
}