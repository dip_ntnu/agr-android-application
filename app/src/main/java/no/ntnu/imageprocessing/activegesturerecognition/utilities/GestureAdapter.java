package no.ntnu.imageprocessing.activegesturerecognition.utilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import no.ntnu.imageprocessing.activegesturerecognition.R;

/**
 * Created by Tor-Martin Holen on 14-Nov-17.
 */

public class GestureAdapter extends ArrayAdapter {
    public GestureAdapter(@NonNull Context context,int resource,  ArrayList<String> gestures) {
        super(context, resource, gestures);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        String name = (String) getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_template, parent, false);
        }

        TextView gestureName = convertView.findViewById(R.id.listItemGestureName);
        gestureName.setText(name);
        return convertView;
    }
}
