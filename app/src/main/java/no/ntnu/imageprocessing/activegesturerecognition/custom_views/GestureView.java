package no.ntnu.imageprocessing.activegesturerecognition.custom_views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import no.ntnu.imageprocessing.activegesturerecognition.data_handling.ImageData;
import no.ntnu.imageprocessing.activegesturerecognition.data_handling.PixelData;
import no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil;

import static org.opencv.core.Core.BORDER_CONSTANT;
import static org.opencv.core.Core.add;
import static org.opencv.core.Core.copyMakeBorder;
import static org.opencv.core.Core.magnitude;
import static org.opencv.core.Core.normalize;

/**
 * Created by Tor-Martin Holen on 19-Oct-17.
 */

public class GestureView extends View{
    private Paint paint = new Paint();
    private Path path = new Path();
    private ImageData imageData;
    private Canvas canvas = new Canvas();
    private Paint cPaint = new Paint(Paint.DITHER_FLAG);
    private Bitmap bitmap;
    private ArrayList<RectF> referencePoints = new ArrayList<>();

    private float canvasSize;
    private String gesture;
    private boolean drawn;
    private String pathToLastGesture = "";

    private MODE mode = MODE.GESTURE_DATA_GENERATION;



    public enum MODE{
        GESTURE_FT_GENERATION,
        GESTURE_DATA_GENERATION,
        GESTURE_RECOGNITION
    }

    public GestureView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setFocusable(true);
        setFocusableInTouchMode(true);
        drawn = false;
    }

    public void setMode(MODE m){
        mode = m;
    }

    public void setGesture(String gesture) {
        this.gesture = gesture;
    }

    private void configurePaintSettings() {
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(canvasSize / 100);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);

        cPaint.setColor(Color.BLACK);
        //cPaint.setColor(Color.argb(255,128,128,255));
        //canvas.drawARGB(255,0,255,128);
        cPaint.setAntiAlias(true);
        cPaint.setStrokeWidth(canvasSize / 100);
        cPaint.setStyle(Paint.Style.FILL);
        cPaint.setStrokeJoin(Paint.Join.ROUND);
        cPaint.setStrokeCap(Paint.Cap.ROUND);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        canvasSize = w;
        configurePaintSettings();

        Log.d("Canvas info", "W:" + w + ", H:" + h);
        ArrayList<PixelData> refPoint = calculateReferencePoints(3, w, h);
        restoreCanvas();
        imageData = new ImageData(refPoint);
    }

    private ArrayList<PixelData> calculateReferencePoints(int gridSize, int w, int h) {
        ArrayList<PixelData> pixelData = new ArrayList<>();

        if (w != h) {
            try {
                throw new Exception("The DrawView's Width must always be equal to it's height");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int margin = w / 8;
        int circleDiameter = w / 20;
        int posSteps = (w - 2 * margin) / (gridSize - 1);

        for (int i = 0; i < gridSize; i++) {
            int left = i * posSteps + margin - circleDiameter / 2;
            int right = left + circleDiameter;

            for (int j = 0; j < gridSize; j++) {
                int top = j * posSteps + margin - circleDiameter / 2;
                int bottom = top + circleDiameter;
                RectF rectF = new RectF(left, top, right, bottom);
                referencePoints.add(rectF); // To avoid these calculations again

                int x = (right + left) / 2;
                int y = (bottom + top) / 2;
                pixelData.add(new PixelData(x, y, 0));
            }
        }
        return pixelData;
    }

    private void restoreReferencePoints() {
        for (RectF rf : referencePoints) {
            canvas.drawOval(rf, cPaint);
        }
    }

    private void restoreCanvas() {
        clearCanvas();
        //canvas.drawARGB(255,0,255,128);
        restoreReferencePoints();
        drawn = false;
    }

    private void clearCanvas() {
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        // Checks for the event that occurs
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                restoreCanvas();
                path.reset();
                path.moveTo(x, y);
                paint.setColor(Color.BLACK);
                return true;
            case MotionEvent.ACTION_MOVE:
                x = restrictToValidCoordinates(x);
                y = restrictToValidCoordinates(y);
                imageData.addData(x, y, SystemClock.elapsedRealtime());
                path.lineTo(x, y);
                canvas.drawPath(path, paint);

                break;
            case MotionEvent.ACTION_UP:
                ArrayList<PixelData> pds = imageData.getData();
                clearCanvas();

                for (int i = 1; i < pds.size(); i++) {
                    PixelData pdfPrev = pds.get(i - 1);
                    PixelData pd = pds.get(i);

                    setColorByTime(pd.getT(), 1500);

                    path.reset();
                    path.moveTo(pdfPrev.getX(), pdfPrev.getY());
                    path.lineTo(pd.getX(), pd.getY());
                    canvas.drawPath(path, paint);
                    invalidate();
                }
                drawn = true;
                saveImage();

                switch (mode) {
                    case GESTURE_FT_GENERATION:
                        generateMagnitudeImage();
                        break;
                    case GESTURE_DATA_GENERATION:
                        break;
                    case GESTURE_RECOGNITION:
                        //TODO handle this case depending on what resources we need.
                        break;
                }

                imageData.printData();
                imageData.reset();
                restoreReferencePoints();
                //samplePathData();
            default:
                return false;
        }
        // Force a view to draw again
        postInvalidate();
        return true;
    }


    public String getPathToLastGesture() {
        return pathToLastGesture;
    }

    private void setColorByTime(float duration, float modulusTime) {
        int colorValue = Math.round(((245 / modulusTime) * duration) % 245)+10;
        Log.d("ColorInfo", "ColorValue:" + colorValue + ", Duration:" + duration + ", MaxTime:" + modulusTime);
        paint.setColor(Color.argb(255, colorValue, colorValue, colorValue));
    }

    public void saveImage() {
        FileOutputStream fileOutputStream = null;
        try {
            File file = DirUtil.getGestureDir(gesture);
            Log.d("SaveImg", "Path: " + file.getPath());
            Log.d("SaveImg", "Number of files in path: " + file.listFiles().length);
            pathToLastGesture = file.getPath() + "/" + file.listFiles().length + ".jpg";
            Log.d("SaveImg", "Path: " + pathToLastGesture);
            fileOutputStream = new FileOutputStream(pathToLastGesture);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Bitmap getBitmap(){
        return bitmap;
    }

    public File getPath(){
        return DirUtil.getGestureDir(gesture);
    }

    private void generateMagnitudeImage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File iDir = DirUtil.getGestureDir(gesture);
                File oDir = DirUtil.getGestureDir(gesture+"-Transformation");

                int fileNr = iDir.listFiles().length -1; // Original image
                String inputImagePath = iDir.getPath() + File.separator + fileNr + ".jpg";
                String outputImagePath = oDir.getPath() + File.separator + fileNr + ".jpg";

                try {
                    Mat image = Imgcodecs.imread(inputImagePath, Imgcodecs.IMREAD_GRAYSCALE);
                    Mat magnitudeImage = obtainMagnitudeSpectrum(image);
                    Imgcodecs.imwrite(outputImagePath, magnitudeImage);
                } catch (UnsatisfiedLinkError e){
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private Mat obtainMagnitudeSpectrum(Mat I) {
        //Ensure image is in right format
        I.convertTo(I, CvType.CV_64FC1);

        // Check what is closest optimal image dimensions,
        // documentation says multiple of numbers 2,3,5 is usually fastest
        int m = Core.getOptimalDFTSize(I.rows());
        int n = Core.getOptimalDFTSize(I.cols());

        // Creates a padded image according to optimal size
        Mat padded = new Mat(new Size(n, m), CvType.CV_64FC1);
        copyMakeBorder(I, padded, 0, m - I.rows(), 0, n - I.cols(), BORDER_CONSTANT);

        // Creates a list with the padded image and a empty image of same size
        List<Mat> planes = new ArrayList<>();
        planes.add(padded);
        planes.add(Mat.zeros(padded.rows(), padded.cols(), CvType.CV_64FC1));

        // Defines a two channel image for storing complex DFT output (Real and Imaginary parts)
        Mat complexI = Mat.zeros(padded.rows(), padded.cols(), CvType.CV_64FC2);

        // Performs DFT storing the result in the complexI variable and splits it as
        // elements in the planes list.
        Core.merge(planes, complexI);
        Core.dft(complexI, complexI);
        Core.split(complexI, planes);

        // Calculates the magnitude by using the real and imaginary parts.
        Mat mag = new Mat(planes.get(0).size(), planes.get(0).type());
        magnitude(planes.get(0), planes.get(1), mag);

        // Converts the magnitude to logarithmic scale
        Mat magI = mag;
        add(magI, Mat.ones(padded.rows(), padded.cols(), CvType.CV_64FC1), magI);
        Core.log(magI, magI);

        // Shifts origo to the centre to the image (the magnitude is symmetric)
        magI = new Mat(magI, new Rect(0, 0, magI.cols() & -2, magI.rows() & -2));
        dftShift(magI);

        // Converts the magnitude to a 8-bit black and white image
        normalize(magI, magI, 0, 255, Core.NORM_MINMAX);
        Mat realResult = new Mat(magI.size(), CvType.CV_8UC1);
        magI.convertTo(realResult, CvType.CV_8UC1);

        return realResult;
    }

    /**
     * Converts a symmetric images origo from top-left (0,0) to center (x/2, y/2)
     *
     * @param I An image that is symmetric
     */
    private void dftShift(Mat I) {
        int cx = I.cols() / 2;
        int cy = I.rows() / 2;

        Mat q0 = new Mat(I, new Rect(0, 0, cx, cy));
        Mat q1 = new Mat(I, new Rect(cx, 0, cx, cy));
        Mat q2 = new Mat(I, new Rect(0, cy, cx, cy));
        Mat q3 = new Mat(I, new Rect(cx, cy, cx, cy));

        Mat tmp = new Mat();
        q0.copyTo(tmp);
        q3.copyTo(q0);
        tmp.copyTo(q3);

        q1.copyTo(tmp);
        q2.copyTo(q1);
        tmp.copyTo(q2);

    }

    private void mLog(String msg) {
        Log.d(this.getClass().getSimpleName(), msg);
    }

    private void logMatInfo(Mat m, String title) {
        mLog(title + " - channels:" + m.channels() + ", size:" + m.rows() + ", " + m.cols());
    }

    /**
     * Restricts a coordinate to never go outside the canvas borders.
     *
     * @param coordinate x or y value
     * @return a value restricted to the canvasSize, i.e. no negative values or values exceeding the canvasSize.
     */
    private float restrictToValidCoordinates(float coordinate) {
        return coordinate > 0 ? ((coordinate <= canvasSize) ? coordinate : canvasSize) : 0;
    }

    private float standardizeData(float coordinate) {
        return ((coordinate / canvasSize) * 500F);
    }

    private void samplePathData() {
        //Toast.makeText(getContext(), "Implement Sample Path Data Functionality", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, cPaint);
        canvas.drawPath(path, paint);
    }

    public boolean isDrawn() {
        return drawn;
    }

    public void emptyCanvas() {
        restoreCanvas();
        path.reset();
        invalidate();
    }
}
