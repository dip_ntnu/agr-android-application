package no.ntnu.imageprocessing.activegesturerecognition.data_handling;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil;

import static no.ntnu.imageprocessing.activegesturerecognition.utilities.DirUtil.getGestureDir;

/**
 * Created by Tor-Martin Holen on 22-Oct-17.
 */

public class ImageData {
    long initT;
    ArrayList<PixelData> data;
    private ArrayList<PixelData> referencePoints;
    public ImageData(ArrayList<PixelData> referencePoints){
        this.referencePoints = referencePoints;
        reset();
    }

    public void addData(float x, float y, long t) {
        if (initT == -1L) {
            initT = t;
        }
        data.add(new PixelData(x, y, t - initT));
    }

    public ArrayList<PixelData> getData() {
        return data;
    }

    public void reset() {
        initT = -1L;
        data = new ArrayList<>();
    }

    public void printData() {
        System.out.println("---");
        for (PixelData pd : data) {
            String line = "x:" + pd.getX() + ", y:" + pd.getY() + ", t:" + pd.getT();
            System.out.println(line);
        }
        System.out.println("Sampled values: " + data.size());
        System.out.println("---");
    }
}